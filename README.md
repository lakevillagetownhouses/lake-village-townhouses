Lake Village Townhomes in Severn Maryland, offers everything you desire in townhome living, and at an affordable price. A great location with outstanding service to our residents, and a variety of floor plans to suit your needs.

Address: 8001 Laketowne Ct, Severn, MD 21144, USA

Phone: 410-551-4444

Website: https://www.lakevillagemd.com

